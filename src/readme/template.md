# Development Environment Managment Project

🔮 Super powers:
- 🐳 Docker
- 🚦 Traefik
- 👓 Portainer

## Requirements

You need to have Docker, Docker Compose and NPM installed to run this project

## Installation

```bash
# Clone the repository
git clone <%= repositoryUrl %>
# Install dependencies
npm install
```

## Get started

Running this project, you use MySQL and Raspbian Stretch containers by default

To start or stop all services (including Traefik and Portainer), run the commands:

```bash
# Start
npm run start
# Stop
npm run stop
```

> See more about commands in [package.json](package.json) > scripts

For Traefik only:

```bash
# Start
npm run compose-up:traefik
# Stop
npm run compose-down:traefik
```

For Portainer only:

```bash
# Start
npm run compose-up:portainer
# Stop
npm run compose-down:portainer
```

For services:

```bash
# Start
npm run compose-up
# Stop
npm run compose-down
```

If shared network is not created:

```bash
docker network create web
```

## Verifying everything works fine

First, initialize database:

```bash
npm run mysql:init
```

Go to http://localhost:8081/ (apache2 from rpi container)

You will get:

> ![screenshot](src/assets/img/apache_works_fine.png)

This page request database to return all MyStation versions registered.

## Configuration

Default configuration:

<%= helpers.table([
  ['Name', 'Value'],
  ['Shared network', '172.19.0.0'],
  ['Traefik container port', '8000'],
  ['Portainer container port', '9000'],
  ['MySQL container address', '172.19.0.3'],
  ['MySQL container port', '3306'],
  ['Raspberry Pi container address', '172.19.0.2'],
  ['Raspberry Pi container port', '8081']
]) %>

## Images

### RPI

#### Build

Run: 
```bash
npm run rpi:build
```

## Dependencies

<details>
<summary>Global</summary>

<%= dependencies %>

</details>

<details>
<summary>Dev</summary>

<%= devDependencies %>

</details>

----

Made with ❤ by Hervé Perchec