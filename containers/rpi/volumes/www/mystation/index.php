<?php
    // Connect to MySQL database
    $dsn = 'mysql:dbname=test;host=mysql_server';
    $user = 'root';
    $password = '';

    try {
        $dbh = new PDO($dsn, $user, $password);
    } catch (PDOException $e) {
        die('Connection failed : ' . $e->getMessage());
    }

    // Get MyStation version from database
    $query = $dbh->prepare('SELECT version FROM mystation');
    $query->execute([]);
    $version = $query->fetchAll();
?>

<html>
    <head>
        <title>Apache 2 | Raspbian Docker container</title>
    </head>
    <body>
        <h1>Hello!</h1>
        <p>Connection with MySQL docker container: <span style="color:green;">OK ✔</span></p>
        <p>MyStation versions:</p>
        <ul>
        <?php
            foreach ($version as $value) {
                ?>
                <li><?= $value['version'] ?></li>
                <?php
            }
        ?>
        </ul>
    </body>
</html>