CREATE DATABASE IF NOT EXISTS test;
CREATE TABLE IF NOT EXISTS test.mystation (
    version VARCHAR(255),
    PRIMARY KEY (version)
);
INSERT INTO test.mystation VALUES ('1.0.0');